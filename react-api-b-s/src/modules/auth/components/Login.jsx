import { useState } from "react";
import { useAuth } from "./Auth";
import { login } from "../core/request";

const Login = () => {

    const [username, setUsername] = useState("kminchelle");
    const [pass, setPass] = useState("0lelplR");
    const [error, setError] = useState("");
    const { saveAuth } = useAuth();

    const onLogin = (event) => {
        event.preventDefault();
        login(username, pass).then((response) => {
            setError("");
            saveAuth(response.data.token)
            window.alert("Login Successful!")
        }).catch((error) => {

            setError(error.response.data.message);
        })
    }

    


    return <div className="container">
    <div className="loginBox">
    <img className="user" src="https://i.ibb.co/yVGxFPR/2.png" height="100px" width="100px" alt="User Icon" />
    <h3>Sign in here</h3>
    <form action="login.php" method="post">
      <div className="inputBox">
        <input id="Username" type="text" name="Username" onChange={(e)=>setUsername(e.target.value)} placeholder="Username" />
        <input id="password" type="password" name="Password" onChange={(e)=>setPass(e.target.value)} placeholder="Password" />
      </div>
      <input onClick={onLogin} type="submit" value="Login" />
    </form>
    <a href="#">Forget Password<br /></a>
    <div className="text-center text-danger">{error}</div>
      <p style={{ color: '#59238F' }}>Sign-Up</p>
    </div>
  </div>
  ;

}



export default Login;
