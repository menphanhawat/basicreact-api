import {useEffect} from "react";
import {useAuth} from "./Auth";
import {getUser} from "../core/request";


const AuthInit = ({children}) => {
    const {auth, setUser, logout} = useAuth();

    useEffect(() => {
        const getCurrentUser = () => {
            getUser().then((response) => {
                setUser(response.data)
            }).catch(() => {
                logout();
             
            })
        }
        getCurrentUser();
        // eslint-disable-next-line
    }, [auth]);
    return children;

}

export {AuthInit}