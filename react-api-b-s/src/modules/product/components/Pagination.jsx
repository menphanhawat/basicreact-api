import { useState } from 'react';
import Pagination from 'react-bootstrap/Pagination';


const PaginationBasic = ({ total, onChangePage }) => {
    const [active, setActive] = useState(1);


    let items = [];
    for (let number = 1; number <= total; number++) {
        items.push(
            <Pagination.Item onClick={() => {
                setActive(number);
                onChangePage && onChangePage(number);
            }} key={number} active={number === active}>
                {number}
            </Pagination.Item>,
        );
    }

    return <Pagination size="lg">{items}</Pagination>
}


export default PaginationBasic;