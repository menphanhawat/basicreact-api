import {useParams} from "react-router-dom";
import {useEffect, useState} from "react";
import { getProductById } from "../core/requst";

const ProductDetail = () => {
    const {id} = useParams();
    const [product, setProduct] = useState({});


    useEffect(() => {
        getProductById(id).then((response)=>{
            setProduct(response.data);
        })
    }, [id]);

    if (!product.id){
        return  <div className="text-center fs-6">
            Loading...
        </div>
    }

    return <div className="container-fluid d-flex justify-content-center">

       <div className="mx-auto">
           <img className="w-100" src={product.thumbnail} alt=""/>
           <h2>  {product.title}</h2>
           <p>{product.description}</p>

       </div>

    </div>
}
export default ProductDetail;